import {createRouter, createWebHashHistory} from 'vue-router';
import EventBus from '@/setting/event-bus';
import { store } from "@/store";
import master from "@/router/master.js";
import purchase from "@/router/purchase.js";
import sales from "@/router/sales.js";
import warehouse from "@/router/warehouse.js";
// import inventory from "@/router/inventory.js";
import finance from "@/router/finance.js";
import reports from "@/router/reports.js";

const routes = [
	{
		path: '/',
		name: 'Dashboard',
		exact: true,
		meta: {requiresAuth: false},
		component: () => import('@/views/home/Dashboard')
	},
	{
		path: '/setting',
		name: 'Setting',
		meta: {requiresAuth: true},
		component: () => import('@/views/profile/Setting')
	},
	{
		path: '/login',
		name: 'Login',
		component: () => import('@/views/auth/Login')
	},
	{
		path: '/access',
		name: 'Access',
		component: () => import('@/views/auth/Access')
	},
	{
		path: '/error',
		name: 'Error',
		component: () => import('@/views/auth/Error')
	},
	{
		path: '/help',
		name: 'Help',
		component: () => import('@/views/auth/Help')
	},
	{
		path: '/:pathMatch(.*)*',
		name: 'NotFound',
		component: () => import('@/views/auth/NotFound')
	},
	{
		path: '/sample/header-detail',
		name: 'header detail',
		component: () => import('@/views/sample/header-detail')
	},
	{
		path: '/setting/user',
		name: 'user',
		component: () => import('@/views/setting/user')
	},
	{
		path: '/setting/user/form',
		name: 'user-form',
		component: () => import('@/views/setting/user/form')
	},
	{
		path: '/profile',
		name: 'profile',
		component: () => import('@/views/setting/user/profile')
	},
	{
		path: '/setting/role',
		name: 'role',
		component: () => import('@/views/setting/role')
	},
	{
		path: '/setting/role/form',
		name: 'role-form',
		component: () => import('@/views/setting/role/form')
	},
	{
		path: '/setting/role-authorization',
		name: 'role-authorization',
		component: () => import('@/views/setting/role-authorization')
	},
	{
		path: '/setting/role-authorization/form',
		name: 'role-authorization-form',
		component: () => import('@/views/setting/role-authorization/form')
	},
	{
		path: '/setting/protect-data',
		name: 'protect-data',
		component: () => import('@/views/setting/protect-data')
	},
	{
		path: '/setting/protect-data/form',
		name: 'protect-data-form',
		component: () => import('@/views/setting/protect-data/form')
	},
	{
		path: '/sample/header-detail/form',
		name: 'header detail form',
		component: () => import('@/views/sample/header-detail/form')
	},
	{
		path: '/sample/params',
		name: 'params',
		component: () => import('@/views/sample/params')
	},
].concat(master).concat(purchase).concat(sales).concat(warehouse).concat(finance).concat(reports);

const router = createRouter({
	history: createWebHashHistory(),
	routes,
	scrollBehavior () {
		return { left: 0, top: 0 };
	}
});

router.beforeEach((to, from, next) => {
	const isAuth = store.state.token;
	if (to.name !== "Login" && isAuth == "") {
		router.push({ path: '/login' });
		return;
	}

	if(store.state.blockedPanelLogin){
		EventBus.emit("show-locked-panel-login","Session Expired");
		return
	}
	if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.isAuthenticated) {
      next("/access");
    }
  }

  next();
});
export default router;
