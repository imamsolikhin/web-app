const warehouse = [
	//GROUP INVENTORY
	{
		path: '/warehouse/adjustment-in',
		name: 'adjustment in',
		component: () => import('@/views/warehouse/adjustment-in'),
	},
	{
		path: '/warehouse/adjustment/adjustment-in-draft',
		name: 'warehouse',
		component: () => import('@/views/master/approval-reason'),
	},
];

export default warehouse;
