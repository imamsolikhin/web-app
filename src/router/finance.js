const finance = [
	//GROUP FINANCE
	{
		path: '/finance/payment-request',
		name: 'finance payment request',
		component: () => import('@/views/finance/payment-request'),
	},
	{
		path: '/finance/payment-request/form',
		name: 'finance payment request form',
		component: () => import('@/views/finance/payment-request/form'),
	},
];

export default finance;
