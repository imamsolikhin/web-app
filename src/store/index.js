import { createStore } from 'vuex'
import { removeAuth, getToken, getPeriodMonth, getPeriodYear, getName, getEmail, getDefaultVatPercent, getDefaultVatDivision, getDefaultCurrencyCode} from '@/utils/cookies'
import EventBus from '@/setting/event-bus';

export const checkErrorResponse = (err) => {
  switch (err.response.status) {
    case 500:
      if(err.response.data.message == "Session Expired"){
        store.state.blockedPanel = true;
        EventBus.emit("show-locked-panel-login",err.response.data.message);
      } else if(err.response.data.message == "Invalid username or password"){
        store.state.blockedPanel = true;
        EventBus.emit("show-locked-panel-login",err.response.data.message);
      }
      break;
    case 502:
      if(err.response.data){
        EventBus.emit("show-warn-notif",err.response.data.message);
      }else{
        EventBus.emit("show-warn-notif",err.response.message);
      }
      console.log('502 error');
      break;
    case 400:
      EventBus.emit("show-warn-notif","Data not found!");
      console.log('400 error');
      break;
    case 401:
      console.log('401 error');
      break;
    default:
      console.log('some other error');
      break;
  }
}

export const RemoveAuth = () => {
  removeAuth()
}

const getCurrentDate = (isTime) => {
  if(isTime){
    return ("0" + new Date().getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear()+" "+new Date().getHours() + ":" + new Date().getMinutes();
  }
  return ("0" + new Date().getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear();
}

const getPeriodDate = (isPeriod,isTime) => {
  if(isTime){
    if(isPeriod){
      return ("0" + new Date(getPeriodYear(), getPeriodMonth()+1).getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear()+" "+new Date().getHours() + ":" + new Date().getMinutes();
    }
    return ("0" + new Date(getPeriodYear(), getPeriodMonth()+1, 0).getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear()+" "+new Date().getHours() + ":" + new Date().getMinutes();
  }
  if(isPeriod){
    return ("0" + new Date(getPeriodYear(), getPeriodMonth()+1).getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear();
  }
  return ("0" + new Date(getPeriodYear(), getPeriodMonth()+1, 0).getDate()).slice(-2)+"/"+getPeriodMonth()+"/"+getPeriodYear();
}
 
export const store = createStore({
  state: {
    isAuthenticated: false,
    blockedPanel: false,
    token: getToken(),
    currentDate: getCurrentDate(false),
    currentDateTime: getCurrentDate(true),
    periodStartDate: getPeriodDate(true,false),
    periodEndDate: getPeriodDate(false,false),
    periodStartDateTime: getPeriodDate(true,true),
    periodEndDateTime: getPeriodDate(false,true),
    periodMonth: getPeriodMonth(),
    periodYear: getPeriodYear(),
    defaultVatDivision: getDefaultVatDivision(),
    defaultVatPercent: getDefaultVatPercent(),
    defaultCurrencyCode:getDefaultCurrencyCode(),
    menus: [],
    profile: {
      name: getName(),
      email: getEmail()
    },
  },
  getters: {
    getDate(state) {
      return state
    },
    getToken(state) {
      return state.token
    },
    getProfile(state) {
      return state.profile
    },
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_PERIOD_MONTH(state, periodMonth) {
      state.periodMonth = periodMonth;
    },
    SET_PERIOD_YEAR(state, periodYear) {
      state.periodYear = periodYear;
    },
    SET_PROFILE(state, profile) {
      state.profile = profile;
    },
    SET_SESSION(state, session) {
      state.session = session;
    },
  },
  actions: {
    storeToken({ commit }, token) {
      commit("SET_TOKEN", token);
    },
    storePeriodMonth({ commit }, periodMonth) {
      commit("SET_PERIOD_MONTH", periodMonth);
    },
    storePeriodYear({ commit }, periodYear) {
      commit("SET_PERIOD_YEAR", periodYear);
    },
    storeProfile({ commit }, profile) {
      commit("SET_PROFILE", profile);
    },
  }
});
