import { authRequest, accountRequest, loadMenuRequest } from '@/api/request/auth/AuthRequest'
import { setAuthentication, setProfile, setSetup } from '@/utils/cookies'
import { store,checkErrorResponse } from "@/store/index.js"

export class AuthControllers {
		authorization(username, password, month, year) {
				let resp = null
				resp = authRequest(username, password)
						.then((response) => {
								this.setLoginDetail(response.data, month, year)
								store.state.blockedPanel = false
						}).catch((err) => {
								checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}

		account() {
				let resp = null
				resp = accountRequest()
						.then((response) => {
								this.setProfiles(response.data?.username, response.data?.role?.name)
								localStorage["menus"]= JSON.stringify(response.data?.menus)
								store.state.blockedPanel = false
						}).catch((err) => {
								if(err.response) checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}

		loadMenu() {
				let resp = null
				resp = loadMenuRequest()
						.then((response) => {
								localStorage["menus"]= JSON.stringify(response.data?.menus)
						}).catch((err) => {
								checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}
 
		setLoginDetail(token, month, year) {
				setAuthentication(token, month, year);
				setSetup(11,1.11,'IDR'); // sementara hadrcode dulu vatDivision
				store.dispatch("storeToken", token);
				store.dispatch("storePeriodMonth", month);
				store.dispatch("storePeriodYear", year);
		}

		setProfiles(nameUser, emailUser) {
				const profile = {
						name: nameUser ?? '',
						email: emailUser ?? ''
				}
				setProfile(nameUser, emailUser)
				store.dispatch("storeProfile", profile);
		}
}
