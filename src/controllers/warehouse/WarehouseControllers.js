import {
    searchWarehouseDefaultRequest,
    lookupWarehouseDefaultRequest,
    dataWarehouseDefaultRequest,
    saveWarehouseDefaultRequest,
    updateWarehouseDefaultRequest,
    deleteWarehouseDefaultRequest
  } from '@/api/request/warehouse/WarehouseRequest'
  import { checkErrorResponse } from '@/store';
  
  export class WarehouseControllers {
   
   
    searchWarehouse(module, data) {
      let resp = null;
      resp = searchWarehouseDefaultRequest(module, data)
        .then((response) => {
          return response.data;
        }).catch((err) => {
          checkErrorResponse(err)
        }).finally(() => {
        })
      return resp;
    }
  
    lookupWarehouse(module, data) {
      let resp = null;
      resp = lookupWarehouseDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      }).catch((err) => {
        checkErrorResponse(err)
      }).finally(() => {
      })
      return resp;
    }
  
    dataWarehouse(module, data) {
      let resp = null;
      resp = dataWarehouseDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      }).catch((err) => {
        checkErrorResponse(err)
      }).finally(() => {
      })
      return resp;
    }
  
    saveWarehouse(module, data) {
      let resp = null;
      resp = saveWarehouseDefaultRequest(module, data)
      .then((response) => {
        return response;
      }).catch((err) => {
        checkErrorResponse(err)
      }).finally(() => {
      })
      return resp;
    }
  
    updateWarehouse(module, data) {
      let resp = null;
      resp = updateWarehouseDefaultRequest(module, data)
      .then((response) => {
        return response;
      }).catch((err) => {
        checkErrorResponse(err)
      }).finally(() => {
      })
      return resp;
    }
  
    deleteWarehouse(module, data) {
      let resp = null;
      resp = deleteWarehouseDefaultRequest(module, data)
      .then((response) => {
        return response;
      }).catch((err) => {
        checkErrorResponse(err)
      }).finally(() => {
      })
      return resp;
    }
  }
  