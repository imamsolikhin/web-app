
export const valdNull = (val,re) => {
  return (val == null)? re:val;
}
export const removeCommas = (val) => {
  return String(val).replace(/,/g, '');
}

export const decimalCurrency = (val) => {
  let num = removeCommas(val);
  return parseFloat(num)?.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

export const formatPrice = (price) => {
  if (!price) return "Rp. 0";
  const result = price.toLocaleString("id");
  return `Rp. ${result}`;
}

export const getStatus = (val,is) => {
  if(is){
    return (parseInt(val) == 1 || val == true) ? 'Active':'In Active';
  }else{
    return (parseInt(val) == 1 || val == true) ? 'success':'warning';
  }
}

export const getParam = (p) => {
  let param = new URLSearchParams(window.location.href.substr(window.location.href.indexOf('?')));
  if(param.has(p)){
    return param.get(p)
  }else{
    return null;
  }
}
