export const searchSalesDefault = (module, data) => {
  return `/${module}/search?`+Object.keys(data).map(key => key + '=' + data[key]).join('&')
}
  
export const lookupSalesDefault = (module, path, data) => {
  return `/${module}/`+path+'?'+Object.keys(data).map(key => key + '=' + data[key]).join('&')
}
 
export const dataSalesDefault = (module, data) => {
  return `/${module}/data?`+Object.keys(data).map(key => key + '=' + data[key]).join('&')
}

export const saveSalesDefault = (module) => {
  return `/${module}/save`
}

export const updateSalesDefault = (module) => {
  return `/${module}/update`
}

export const deleteSalesDefault = (module,data) => {
  return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + data[key]).join('&')
}
