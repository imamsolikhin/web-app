export const searchWarehouseDefault = (module, data) => {
    return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D")
  }
  
  export const lookupWarehouseDefault = (module, path, data) => {
    return `/${module}/`+path+'?'+Object.keys(data).map(key => key + '=' + data[key]).join('&')
  }
  
  export const dataWarehouseDefault = (module, data) => {
    return `/${module}/data?`+Object.keys(data).map(key => key + '=' + data[key]).join('&')
  }
  
  export const saveWarehouseDefault = (module) => {
    return `/${module}/save`
  }
  
  export const updateWarehouseDefault = (module) => {
    return `/${module}/update`
  }
  
  export const deleteWarehouseDefault = (module,data) => {
    return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + data[key]).join('&')
  }
  