import axios from 'axios'
import { Login, Validates, LoadMenu } from '@/api/endpoint/auth/AuthEndpoint'
import { BaseAuthUrl } from '@/api/endpoint/base'
import { store } from "@/store"

export const authRequest = (username, password) => {
  const resp = axios({
    method: 'post',
    url: `${BaseAuthUrl()}${Login()}`,
    data: {
      username: username,
      password: password
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const accountRequest = () => {
  const resp = axios({
    method: 'post',
    url: `${BaseAuthUrl()}${Validates()}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const loadMenuRequest = () => {
  const resp = axios({
    method: 'get',
    url: `${BaseAuthUrl()}${LoadMenu()}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}
