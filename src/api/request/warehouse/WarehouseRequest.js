import axios from 'axios'
import {
  searchWarehouseDefault,
  lookupWarehouseDefault,
  saveWarehouseDefault,
  dataWarehouseDefault,
  updateWarehouseDefault,
  deleteWarehouseDefault } from '@/api/endpoint/warehouse/WarehouseEndpoint';
import { BaseWarehouseUrl } from '@/api/endpoint/base'
import { store } from "@/store/index.js"

export const searchWarehouseDefaultRequest = (module, data) => {
  let resp = null
  resp = axios({
    method: 'GET',
    url: `${BaseWarehouseUrl()}${searchWarehouseDefault(module,data)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const lookupWarehouseDefaultRequest = (module, path, data) => {
  let resp = null
  resp = axios({
    method: 'GET',
    url: `${BaseWarehouseUrl()}${lookupWarehouseDefault(module, path, data)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const dataWarehouseDefaultRequest = (module, data) => {
  let resp = null
  resp = axios({
    method: 'GET',
    url: `${BaseWarehouseUrl()}${dataWarehouseDefault(module,data)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const saveWarehouseDefaultRequest = (module,data) => {
  let resp = null
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseWarehouseUrl()}${saveWarehouseDefault(module)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const updateWarehouseDefaultRequest = (module,data) => {
  let resp = null
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseWarehouseUrl()}${updateWarehouseDefault(module)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}

export const deleteWarehouseDefaultRequest = (module,data) => {
  let resp = null
  resp = axios({
    method: 'POST',
    url: `${BaseWarehouseUrl()}${deleteWarehouseDefault(module,data)}`,
    headers: {
      'Authorization': `Bearer ${store.state.token}`,
    }
  }).then((response) => {
    return response
  })
  return resp
}
