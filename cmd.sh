docker rm $(docker stop $(docker ps -a -q --filter name="e-payment-api" --format="{{.ID}}")) || true
docker build -t "e-payment-api" .
docker run -i -p 8181:8080 --name="e-payment-api" -v $PWD:/app "e-payment-api"
